﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Clinic.Data;
using Clinic.Models;
using System.Text.Json;

namespace Clinic.Controllers
{
    public class AppointmentsController : Controller
    {
        private readonly ClinicContext _context;

        public AppointmentsController(ClinicContext context)
        {
            _context = context;
        }

        // GET: Appointments
        public async Task<IActionResult> Index(bool history = false)
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Problem(statusCode: 401);

            if (_context.Appointment == null) return Problem(statusCode: 500);

            return Json(await _context.Appointment
                .Where(a => (user.IsDoctor ? a.DoctorId : a.PatientId) == user.Id)
                .Where(a => history ? a.StartTime <= DateTime.Now : a.StartTime >= DateTime.Now)
                .OrderBy(a => a.StartTime)
                .Include(a => user.IsDoctor ? a.Patient : a.Doctor).ToListAsync());
        }

        // GET: Appointments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Appointment == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment.Include(a => a.Patient).Include(a => a.Doctor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appointment == null)
            {
                return NotFound();
            }
            
            return Json(appointment);
        }

        // GET: Appointments/Create
        public async Task<IActionResult> Filter(int? SpecialityId)
        {
            if (SpecialityId == null)
            {
                return Json(Array.Empty<int>());
            }

            return Json(await _context.Appointment.
                Include(a => a.Schedule.Doctor).
                Where(a => a.StartTime >= DateTime.Now).
                Where(a => a.Doctor!.SpecialityId == SpecialityId).
                Where(a => a.PatientId == null).
                OrderBy(a => a.StartTime).ToListAsync()
            );
        }

        // POST: Appointments/Book
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Book(int? id)
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Redirect("/Auth/SignIn");

            if (id == null || _context.Appointment == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment
                .FirstOrDefaultAsync(m => m.Id == id);

            if (appointment == null)
            {
                return NotFound();
            }

            if (appointment.PatientId != null)
            {
                return Problem(statusCode: 400);
            }

            appointment.Patient = user;
            _context.Update(appointment);
            await _context.SaveChangesAsync();

            return Json(appointment);
        }

        // POST: Appointments/Book
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Cancel(int? id)
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Redirect("/Auth/SignIn");

            if (id == null || _context.Appointment == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment
                .FirstOrDefaultAsync(m => m.Id == id);

            if (appointment == null)
            {
                return NotFound();
            }

            if (appointment.PatientId != user.Id)
            {
                return Problem(statusCode: 403);
            }

            appointment.Patient = null;
            _context.Update(appointment);
            await _context.SaveChangesAsync();

            return Json(appointment);
        }

        // POST: Appointments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description")] Appointment appointment)
        {

            if (id != appointment.Id)
            {
                return NotFound();
            }

            var dbAppointment = await _context.Appointment
                .FirstOrDefaultAsync(m => m.Id == id);

            if (dbAppointment == null)
            {
                return NotFound();
            }

            if (appointment.Description != dbAppointment.Description) dbAppointment.DescriptionEditTime = DateTime.Now;
            dbAppointment.Description = appointment.Description;

            try
            {
                _context.Update(dbAppointment);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppointmentExists(dbAppointment.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Json(dbAppointment);
        }

        private bool AppointmentExists(int id)
        {
            return (_context.Appointment?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
