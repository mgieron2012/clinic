﻿using Clinic.Data;
using Clinic.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace Clinic.Controllers
{
    public class AuthController : Controller
    {
        private readonly ClinicContext _context;

        public AuthController(ClinicContext context)
        {
            _context = context;
        }

        [IgnoreAntiforgeryToken]
        [HttpPost]
        public async Task<IActionResult> SignUp(string Email, string Password, string FirstName, string LastName)
        {
            User user = new()
            {
                Email = Email,
                Password = Password,
                FirstName = FirstName,
                LastName = LastName,
                IsAdmin = false,
                IsActive = false,
                IsDoctor = false
            };

            _context.Add(user);

            try
            {
                await _context.SaveChangesAsync();
            } catch
            {
                return Problem(statusCode: 451);
            }
            return Json(user);
        }

        [IgnoreAntiforgeryToken]
        [HttpPost]
        public async Task<IActionResult> SignIn(string Email, string Password)
        {
            var user = await _context.User.FirstOrDefaultAsync(m => m.Email == Email && m.Password == Password);
            if (user == null)
            {
                return Problem(statusCode: 400);
            }
            if (!user.IsActive)
            {
                return Problem(statusCode: 451);
            }

            var lastToken = await _context.Token.FirstOrDefaultAsync(t => t.UserID == user.Id);
            if (lastToken != null) _context.Token.Remove(lastToken);

            var token = new Token
            {
                Key = Convert.ToBase64String(Guid.NewGuid().ToByteArray()),
                UserID = user.Id
            };
            _context.Token.Add(token);
            _context.SaveChanges();
            Response.Cookies.Append("token", token.Key);

            return Json(token);
        }
    }
}
