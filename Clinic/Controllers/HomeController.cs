﻿using Clinic.Data;
using Clinic.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Clinic.Controllers
{
    public class HomeController : Controller
    {
        private readonly ClinicContext _context;

        public HomeController(ClinicContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var user = _context.TryAuthorize(Request);
            if(user is null)
            {
                ViewData["IsAuthorized"] = false;
                return View();
            }
            if (user.IsAdmin)
            {
                ViewData["IsAuthorized"] = true;
                return View();
            }
            return RedirectToAction("Index", "Appointments");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}