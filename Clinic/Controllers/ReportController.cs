﻿using Clinic.Data;
using Clinic.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Clinic.Controllers
{
    public class ReportController : Controller
    {
        private readonly ClinicContext _context;

        public ReportController(ClinicContext context)
        {
            _context = context;
        }

        public IActionResult Index(DateTime? from, DateTime? to)
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Redirect("/Auth/SignIn");
            if (!user.IsAdmin) return Problem(statusCode: 403);

            from ??= DateTime.Now;
            to ??= DateTime.Now;

            ViewData["from"] = from?.ToString("yyyy-MM-dd");
            ViewData["to"] = to?.ToString("yyyy-MM-dd");
            ViewBag.schedules = new Dictionary<DateTime, Schedule[]>();
            ViewBag.appointmentsCount = new Dictionary<int, int>();

            while(from <= to)
            {
                var schedules = _context.Schedule.Include(s => s.Doctor).Where(s => s.Date == from).ToArray();
                ViewBag.schedules.Add((DateTime)from, schedules);

                foreach(var schedule in schedules)
                {
                    ViewBag.appointmentsCount.Add(schedule.Id, _context.Appointment.Where(a => a.ScheduleId == schedule.Id)
                        .Where(a => a.PatientId != null).Count());
                }

                from = ((DateTime)from).AddDays(1);
            }
            return Json(ViewData);
        }
    }
}
