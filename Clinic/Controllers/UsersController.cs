﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Clinic.Data;
using Clinic.Models;

namespace Clinic.Controllers
{
    public class UsersController : Controller
    {
        private readonly ClinicContext _context;

        public UsersController(ClinicContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Problem(statusCode: 401);
            if (!user.IsAdmin) return Problem(statusCode: 403);
            
            var clinicContext = _context.User.Where(u => !u.IsAdmin).Include(u => u.Speciality).OrderBy(u => u.LastName).ThenBy(u => u.FirstName);
            return Json(await clinicContext.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var _user = _context.TryAuthorize(Request);
            if (_user is null) return Problem(statusCode: 401);
            if (!_user.IsAdmin) return Problem(statusCode: 403);

            if (id == null || _context.User == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .Include(u => u.Speciality)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return Json(user);
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Email,Password,SpecialityId")] User user)
        {
            var _user = _context.TryAuthorize(Request);
            if (_user is null) return Problem(statusCode: 401);
            if (!_user.IsAdmin) return Problem(statusCode: 403);

            if (ModelState.IsValid)
            {
                user.IsDoctor = true;
                user.IsActive = true;
                _context.Add(user);
                await _context.SaveChangesAsync();
                return Json(user);
            }
            return Problem(statusCode: 400);
        }


        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Email,IsActive,SpecialityId")] User user)
        {
            var _user = _context.TryAuthorize(Request);
            if (_user is null) return Problem(statusCode: 401);
            if (!_user.IsAdmin) return Problem(statusCode: 403);

            var dbUser = await _context.User.FindAsync(id);

            if (dbUser is null)
            {
                return NotFound();
            }

            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.Email = user.Email;
            dbUser.IsActive = user.IsActive;
            dbUser.SpecialityId = user.SpecialityId;

            try
            {
                _context.Update(dbUser);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Json(dbUser);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Activate(int id)
        {
            var user = _context.TryAuthorize(Request);
            if (user is null) return Problem(statusCode: 401);
            if (!user.IsAdmin) return Problem(statusCode: 403);

            var dbUser = await _context.User.FindAsync(id);

            if (dbUser is null)
            {
                return NotFound();
            }

            dbUser.IsActive = true;

            try
            {
                _context.Update(dbUser);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(dbUser.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Json(dbUser);
        }

        // POST: Users/Delete/5
        [HttpDelete, ActionName("Delete")]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var _user = _context.TryAuthorize(Request);
            if (_user is null) return Problem(statusCode: 401);
            if (!_user.IsAdmin) return Problem(statusCode: 403);

            if (_context.User == null)
            {
                return Problem("Entity set 'ClinicContext.User'  is null.");
            }
            var user = await _context.User.FindAsync(id);
            if (user != null)
            {
                _context.User.Remove(user);
            }
            
            await _context.SaveChangesAsync();
            return Json(new { });
        }

        private bool UserExists(int id)
        {
          return (_context.User?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
