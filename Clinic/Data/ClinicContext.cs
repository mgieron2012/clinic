﻿using Microsoft.EntityFrameworkCore;
using Clinic.Models;

namespace Clinic.Data
{
    public class ClinicContext : DbContext
    {
        public ClinicContext (DbContextOptions<ClinicContext> options)
            : base(options)
        {}

        public DbSet<Clinic.Models.Appointment> Appointment { get; set; } = default!;
        public DbSet<Clinic.Models.User> User { get; set; } = default!;
        public DbSet<Clinic.Models.Token> Token { get; set; } = default!;
        public DbSet<Clinic.Models.Speciality> Speciality { get; set; } = default!;
        public DbSet<Clinic.Models.Schedule> Schedule { get; set; } = default!;
        
        public User? TryAuthorize(HttpRequest request)
        {
            string? token = request.Headers["Authorization"];
            try
            {
                token = token?.Split(" ")[1];
                return Token.Include("User").FirstOrDefault(t => t.Key!.Equals(token))?.User;
            } catch {
                return null;
            }
        }        
    }
}
