﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clinic.Migrations
{
    /// <inheritdoc />
    public partial class Init2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_User_DoctorId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_User_PatientId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Schedule_User_DoctorId",
                table: "Schedule");

            migrationBuilder.DropForeignKey(
                name: "FK_Token_User_UserID",
                table: "Token");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Speciality_SpecialityId",
                table: "User");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User",
                table: "User");

            migrationBuilder.RenameTable(
                name: "User",
                newName: "Users");

            migrationBuilder.RenameIndex(
                name: "IX_User_SpecialityId",
                table: "Users",
                newName: "IX_Users_SpecialityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Users_DoctorId",
                table: "Appointment",
                column: "DoctorId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Users_PatientId",
                table: "Appointment",
                column: "PatientId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Schedule_Users_DoctorId",
                table: "Schedule",
                column: "DoctorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Token_Users_UserID",
                table: "Token",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Speciality_SpecialityId",
                table: "Users",
                column: "SpecialityId",
                principalTable: "Speciality",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Users_DoctorId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Users_PatientId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Schedule_Users_DoctorId",
                table: "Schedule");

            migrationBuilder.DropForeignKey(
                name: "FK_Token_Users_UserID",
                table: "Token");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Speciality_SpecialityId",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "User");

            migrationBuilder.RenameIndex(
                name: "IX_Users_SpecialityId",
                table: "User",
                newName: "IX_User_SpecialityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_User",
                table: "User",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_User_DoctorId",
                table: "Appointment",
                column: "DoctorId",
                principalTable: "User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_User_PatientId",
                table: "Appointment",
                column: "PatientId",
                principalTable: "User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Schedule_User_DoctorId",
                table: "Schedule",
                column: "DoctorId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Token_User_UserID",
                table: "Token",
                column: "UserID",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Speciality_SpecialityId",
                table: "User",
                column: "SpecialityId",
                principalTable: "Speciality",
                principalColumn: "Id");
        }
    }
}
