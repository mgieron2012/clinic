﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public int? PatientId { get; set; }
        public int? DoctorId { get; set; }
        public int ScheduleId { get; set; }
        public string? Description { get; set; }
        [Required]
        public DateTime? StartTime { get; set; }
        public DateTime? DescriptionEditTime { get; set; }

        [ForeignKey("PatientId")]
        public User? Patient { get; set; }
        [ForeignKey("DoctorId")]
        public User? Doctor { get; set; }
        [ForeignKey("ScheduleId")]
        public Schedule? Schedule { get; set; }

        public const int DurationInMinutes = 15;
    }
}
