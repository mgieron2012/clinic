﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic.Models
{
    public class Schedule
    {
        public int Id { get; set; }
        [Required, DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required, DisplayName("Start time"), DataType(DataType.Time)]
        public DateTime StartTime { get; set; }
        [Required, DisplayName("End time"), DataType(DataType.Time)]
        public DateTime EndTime { get; set; }
        [Required, DisplayName("Doctor")]
        public int DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public User? Doctor { get; set; }
    }
}
