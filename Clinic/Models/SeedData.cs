﻿using Microsoft.EntityFrameworkCore;
using Clinic.Data;

namespace Clinic.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ClinicContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ClinicContext>>()))
            {
                if (context.User.Any())
                {
                    return;
                }

                context.User.Add(
                    new User
                    {
                        IsActive = true,
                        Email = "admin",
                        Password = "admin",
                        IsAdmin = true
                    }
                );

                context.Speciality.AddRange(
                    new Speciality { Name = "Cardiology" },
                    new Speciality { Name = "Family Medicine" },
                    new Speciality { Name = "Neurology" },
                    new Speciality { Name = "Pediatrics" },
                    new Speciality { Name = "Psychiatry" },
                    new Speciality { Name = "Respirology" }
                );

                //context.Appointment.AddRange(
                //    new Appointment
                //    {
                //        Description = "Appointment 1",
                //        PatientId = 1,
                //        DoctorId = 2,
                //        StartTime = DateTime.Now,
                //    }        
                //);
                context.SaveChanges();
            }
        }
    }
}