﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic.Models
{
    public class Token
    {
        public int ID { get; set; }
        public string? Key { get; set; }
        public int UserID { get; set; }

        [ForeignKey("UserID")]
        public User? User { get; set; }
    }
}
