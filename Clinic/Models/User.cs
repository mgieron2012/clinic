﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Clinic.Models
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }
        [DisplayName("First name"), Required]
        public string? FirstName { get; set; }
        [DisplayName("Last name"), Required]
        public string? LastName { get; set; }
        [Required]
        public string? Email { get; set; }
        [Required, JsonIgnore]
        public string? Password { get; set; }
        public bool IsAdmin { get; set; } = false;
        [DisplayName("Activated")]
        public bool IsActive { get; set; }
        public bool IsDoctor { get; set; }
        public int? SpecialityId { get; set; }
        
        [ForeignKey("SpecialityId")]
        public Speciality? Speciality { get; set; }
        
        [NotMapped]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
