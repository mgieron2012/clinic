﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Clinic.Data;
using Clinic.Models;

namespace Clinic.Views
{
    public class SchedulesController : Controller
    {
        private readonly ClinicContext _context;

        public SchedulesController(ClinicContext context)
        {
            _context = context;
        }

        // GET: Schedules
        public async Task<IActionResult> Index(DateTime? date)
        {
            date ??= DateTime.Now.Date;
            var clinicContext = _context.Schedule.Include(s => s.Doctor).OrderBy(s=>s.StartTime).Where(s => s.Date.Equals(date));
            return Json(await clinicContext.ToListAsync());
        }

        // GET: Schedules/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Schedule == null)
            {
                return NotFound();
            }

            var schedule = await _context.Schedule
                .Include(s => s.Doctor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schedule == null)
            {
                return NotFound();
            }

            return Json(schedule);
        }

        // POST: Schedules/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Date,StartTime,EndTime,DoctorId")] Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                if (schedule.StartTime >= schedule.EndTime) return Problem(statusCode: 400);
                _context.Add(schedule);
                await _context.SaveChangesAsync();
                GenerateAppointments(schedule);
                await _context.SaveChangesAsync();

                return Json(schedule);
            }
            return Problem(statusCode: 400);
        }

        // POST: Schedules/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Date,StartTime,EndTime,DoctorId")] Schedule schedule)
        {
            if (id != schedule.Id)
            {
                return NotFound();
            }

            if(ScheduleHasAppointents(id))
            {
                return Problem(statusCode: 450);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schedule);
                    DeleteAppointments(schedule);
                    GenerateAppointments(schedule);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ScheduleExists(schedule.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(schedule);
            }
            return Problem(statusCode: 400);
        }

        // POST: Schedules/Delete/5
        [HttpDelete, ActionName("Delete")]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (_context.Schedule == null)
            {
                return Problem("Entity set 'ClinicContext.Schedule'  is null.");
            }

            if (ScheduleHasAppointents(id))
            {
                return Problem(statusCode: 450);
            }

            var schedule = await _context.Schedule.FindAsync(id);
            if (schedule != null)
            {
                _context.Schedule.Remove(schedule);
            }
            
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Copy(DateTime fromDay, DateTime toDay)
        {
            if(_context.Schedule.Where(s => s.Date == toDay).Any())
            {
                return Problem("Destination day cannot contain any planned event", statusCode: 400);
            }

            var events = _context.Schedule.Where(s => s.Date == fromDay).ToArray();
            foreach (var ev in events){
                var schedule = new Schedule
                {
                    DoctorId = ev.DoctorId,
                    Date = toDay,
                    StartTime = ev.StartTime,
                    EndTime = ev.EndTime
                };
                _context.Schedule.Add(schedule);
                await _context.SaveChangesAsync();
                
                GenerateAppointments(schedule);
            }
            await _context.SaveChangesAsync();
            return Json(new {});
        }

        private bool ScheduleExists(int id)
        {
            return (_context.Schedule?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        private bool ScheduleHasAppointents(int id)
        {
            return _context.Appointment.Where(a => a.ScheduleId == id && a.PatientId != null).Any();
        }

        private void GenerateAppointments(Schedule schedule)
        {
            var nextAppointmentStartTime = schedule.Date.Add(schedule.StartTime.TimeOfDay);
            var scheduleEndDateTime = schedule.Date.Add(schedule.EndTime.TimeOfDay);

            while (nextAppointmentStartTime.AddMinutes(Appointment.DurationInMinutes) <= scheduleEndDateTime)
            {
                _context.Add(new Appointment
                {
                    DoctorId = schedule.DoctorId,
                    StartTime = nextAppointmentStartTime,
                    ScheduleId = schedule.Id,
                });
                nextAppointmentStartTime = nextAppointmentStartTime.AddMinutes(Appointment.DurationInMinutes);
            }
        }

        private void DeleteAppointments(Schedule schedule)
        {
            _context.Appointment.RemoveRange(_context.Appointment.Where(a => a.ScheduleId == schedule.Id));
        }
    }
}
