import { Alert } from "@mui/material";

const defaultMessages = {
    "200": "Success!",
    "400": "Invalid data",
    "401": "Unauthorized",
    "403": "Action not permitted",
    "500": "Unexpected error occurred"
}

const ActionResultMessage = (props: { status?: number }) => {
    return <>
        {props.status === 200 && <Alert severity="success">{defaultMessages['200']}</Alert>}
        {props.status === 400 && <Alert severity="warning">{defaultMessages['400']}</Alert>}
        {props.status === 401 && <Alert severity="error">{defaultMessages['401']}</Alert>}
        {props.status === 403 && <Alert severity="error">{defaultMessages['403']}</Alert>}
        {props.status === 500 && <Alert severity="error">{defaultMessages['500']}</Alert>}
    </>
}

export default ActionResultMessage;