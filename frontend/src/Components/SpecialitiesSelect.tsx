import React from 'react';
import { FormControl, InputLabel, Select } from '@mui/material';

const specialities = [
    { id: 1, name: "Cardiology" },
    { id: 2, name: "Family Medicine" },
    { id: 3, name: "Neurology" },
    { id: 4, name: "Pediatrics" },
    { id: 5, name: "Psychiatry" },
    { id: 6, name: "Respirology" }
]

const SpecialitiesSelect = (props: { defaultValue?: number }) => {
    return <FormControl fullWidth>
        <InputLabel shrink htmlFor="spec-select">
            Speciality
        </InputLabel>
        <Select native defaultValue={props.defaultValue} name="SpecialityId" label="Speciality" id="spec-select">
            {specialities.map(({ id, name }) =>
                <option value={id}>{name}</option>
            )}
        </Select>
    </FormControl>
}

export default SpecialitiesSelect;