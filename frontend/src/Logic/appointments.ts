import { ActionFunction, LoaderFunction, redirect } from "react-router-dom";
import { simpleGETFetch, ENDPOINT, simplePOSTFetch } from "./simpleFetch";
import { SystemUser } from "./users";
import { Schedule } from "./schedules";

export type Appointment = {
    id: number;
    patientId: number;
    doctorId: number;
    scheduleId: number;
    description: string;
    startTime: string;
    patient: SystemUser;
    doctor: SystemUser;
    schedule: Schedule;
}

export type Appointments = Array<Appointment>;

export const appointmentsLoader: LoaderFunction = async ({ request }) => {
    const searchParams = new URL(request.url).searchParams;
    const response = await simpleGETFetch<Array<Appointment>>(ENDPOINT.APPOINTMENTS_LIST, undefined, searchParams);

    return { appointments: response.data, history: searchParams.get("history") === "true" };
}

export const appointmentLoader: LoaderFunction = async ({ params }) => {
    const response = await simpleGETFetch<Appointment>(ENDPOINT.APPOINTMENTS_DETAILS, params.appointmentId);

    return response.data;
}

export const appointmentsFilterLoader: LoaderFunction = async ({ request }) => {
    const searchParams = new URL(request.url).searchParams;
    const response = await simpleGETFetch<Array<Appointment>>(ENDPOINT.APPOINTMENTS_FILTER, undefined, searchParams);

    return response.data;
}

export const appointmentBookAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Appointment>(ENDPOINT.APPOINTMENTS_BOOK, data, data.get("Id") as any);

    if (response.status === 200) return redirect(`/appointment/${response.data.id}`);
    return response.status;
}

export const appointmentCancelAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Appointment>(ENDPOINT.APPOINTMENTS_CANCEL, data, data.get("Id") as any);

    return response.status;
}

export const appointmentEditAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Appointment>(ENDPOINT.APPOINTMENTS_EDIT, data, data.get("Id") as any);

    return response.status;
}