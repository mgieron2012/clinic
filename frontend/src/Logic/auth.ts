import { ActionFunction, redirect } from "react-router-dom";
import { ENDPOINT, simpleGETFetch, simplePOSTFetch } from "./simpleFetch";
import User from "./user";
import { SystemUser } from "./users";

export type Token = {
    id: number;
    key: string;
    userId: number;
    user: SystemUser;
}

export const signInAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Token>(ENDPOINT.SIGN_IN, data);

    if(response.status === 200){
        User.signIn(response.data);
        return redirect("/");
    } else {
        return response.status;
    }
}

export const signUpAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Token>(ENDPOINT.SIGN_UP, data);

    if(response.status === 200){
        return redirect("/signIn");
    } else {
        return response.status;
    }
}
