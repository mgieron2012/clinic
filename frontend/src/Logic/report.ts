import { LoaderFunction } from "react-router-dom";
import { simpleGETFetch, ENDPOINT } from "./simpleFetch";
import { Schedule } from "./schedules";

export type Report = {
    from: string,
    to: string,
    schedules: {
        [key: string]: Array<Schedule>
    },
    appointmentsCount: {
        [key: string]: number
    }
};

export const reportLoader: LoaderFunction = async ({ request }) => {
    const searchParams = new URL(request.url).searchParams;
    const response = await simpleGETFetch<Report>(ENDPOINT.REPORT_GET, undefined, searchParams);

    return response.data;
}