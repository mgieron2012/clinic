import { ActionFunction, LoaderFunction, redirect } from "react-router-dom";
import { simpleGETFetch, ENDPOINT, simplePOSTFetch, simpleDELETEFetch } from "./simpleFetch";
import { SystemUser } from "./users";

export type Schedule = {
    id: number;
    date: string;
    startTime: string;
    endTime: string;
    doctorId: number;
    doctor: SystemUser;
}

export const schedulesLoader: LoaderFunction = async ({ request }) => {
    const searchParams = new URL(request.url).searchParams;
    const response = await simpleGETFetch<Array<Schedule>>(ENDPOINT.SCHEDULES_LIST, undefined, searchParams);

    return { schedules: response.data, date: searchParams.get("date") || new Date().toISOString().substring(0, 10) };
}

export const scheduleLoader: LoaderFunction = async ({ params }) => {
    const response = simpleGETFetch<Schedule>(ENDPOINT.SCHEDULES_DETAILS, params.scheduleId);
    const usersResponse = simpleGETFetch<Array<SystemUser>>(ENDPOINT.USERS_LIST);

    return { users: (await usersResponse).data, schedule: (await response).data };
}

export const scheduleAddAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<Schedule>(ENDPOINT.SCHEDULES_ADD, data);

    if (response.status === 200) return redirect(`/schedule/${response.data.id}`);
    return response.status;
}

export const scheduleAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    if (data.get('intent') === "delete") {
        const response = await simpleDELETEFetch(ENDPOINT.SCHEDULES_DELETE, data.get('Id') as any);

        if (response.status === 200) {
            return redirect("/schedules");
        }

        return response.status;
    } else {
        const response = await simplePOSTFetch<Schedule>(ENDPOINT.SCHEDULES_EDIT, data, data.get('Id') as any);

        return response.status;
    }
}

export const scheduleCopyAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch(ENDPOINT.SCHEDULES_COPY, data);

    return response.status;
}