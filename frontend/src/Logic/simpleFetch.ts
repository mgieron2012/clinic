import { URLSearchParams } from "url";
import user from "./user";

const url = "https://localhost:7259";

enum ENDPOINT {
    SIGN_IN = "Auth/SignIn",
    SIGN_UP = "Auth/SignUp",
    APPOINTMENTS = "Appointments",
    USERS_LIST = "Users",
    USERS_ACTIVATE = "Users/Activate",
    USERS_DETAILS = "Users/Details",
    USERS_EDIT = "Users/Edit",
    USERS_DELETE = "Users/Delete",
    USERS_ADD = "Users/Create",
    SCHEDULES_LIST = "Schedules",
    SCHEDULES_ADD = "Schedules/Create",
    SCHEDULES_EDIT = "Schedules/Edit",
    SCHEDULES_DELETE = "Schedules/Delete",
    SCHEDULES_DETAILS = "Schedules/Details",
    SCHEDULES_COPY = "Schedules/Copy",
    REPORT_GET = "Report",
    APPOINTMENTS_LIST = "Appointments",
    APPOINTMENTS_FILTER = "Appointments/Filter",
    APPOINTMENTS_DETAILS = "Appointments/Details",
    APPOINTMENTS_BOOK = "Appointments/Book",
    APPOINTMENTS_CANCEL = "Appointments/Cancel",
    APPOINTMENTS_EDIT = "Appointments/Edit",
}

async function simpleGETFetch<DataType>(endpoint: ENDPOINT, id?: number | string, params?: URLSearchParams) {
    const response = await fetch(`${url}/${endpoint}${id ? "/" + id : "" }?${params ? params.toString() : ""}`, {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            Authorization: `Token ${user.token}`,
        }
    });
    return {
        status: response.status,
        data: await response.json() as DataType
    }
};

async function simplePOSTFetch<DataType>(endpoint: ENDPOINT, data: any, id?: number | string) {
    const response = await fetch(`${url}/${endpoint}${id ? "/" + id : ""}`, {
        method: 'POST',
        headers: {
            Authorization: `Token ${user.token}`,
        },
        body: data
    });
    return {
        status: response.status,
        data: await response.json() as DataType
    }
}

async function simpleDELETEFetch(endpoint: ENDPOINT, id: number | string) {
    const response = await fetch(`${url}/${endpoint}/${id}`, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json",
            Authorization: `Token ${user.token}`,
        }
    });
    return {
        status: response.status
    }
}

export { simpleGETFetch, simplePOSTFetch, simpleDELETEFetch, ENDPOINT };
