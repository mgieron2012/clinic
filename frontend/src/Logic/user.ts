import { Token } from "./auth";

class User {
    token: string;
    type: "Admin" | "Patient" | "Doctor";

    constructor() {
        this.token = localStorage.getItem("token") || "";
        this.type = localStorage.getItem("userType") as any || "Admin";
    }

    get isAuthorized() {
        return this.token !== "";
    }

    signIn = (token: Token) => {
        this.token = token.key;
        localStorage.setItem("token", this.token);
        
        this.type = token.user.isAdmin ? "Admin" : (token.user.isDoctor ? "Doctor" : "Patient");
        localStorage.setItem("userType", this.type);
    }

    signOut = () => {
        this.token = "";
        localStorage.removeItem("token");
        localStorage.removeItem("userType");
    }
}

export default new User();
