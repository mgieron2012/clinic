import { ActionFunction, LoaderFunction, redirect } from "react-router-dom";
import { ENDPOINT, simpleDELETEFetch, simpleGETFetch, simplePOSTFetch } from "./simpleFetch";

type Speciality = {
    id: number;
    name: string;
}

export type SystemUser = {
    id: number;
    fullName: string;
    email: string;
    isActive: boolean;
    isDoctor: boolean;
    isAdmin: boolean;
    specialityId: number;
    speciality: Speciality;
    firstName: string;
    lastName: string;
}

export const usersLoader: LoaderFunction = async () => {
    const response = await simpleGETFetch<Array<SystemUser>>(ENDPOINT.USERS_LIST);
    return response.data;
}

export const userLoader: LoaderFunction = async ({ params }) => {
    const response = await simpleGETFetch<SystemUser>(ENDPOINT.USERS_DETAILS, params.userId);
    return response.data;
}

export const activationAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<SystemUser>(ENDPOINT.USERS_ACTIVATE, data, data.get('id') as any);

    return response.status;
}

export const userAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    if (data.get('intent') === "delete") {
        const response = await simpleDELETEFetch(ENDPOINT.USERS_DELETE, data.get('Id') as any);

        if (response.status === 200) {
            return redirect("/users");
        }

        return response.status;
    } else {
        const response = await simplePOSTFetch<SystemUser>(ENDPOINT.USERS_EDIT, data, data.get('Id') as any);

        return response.status;
    }
}

export const userAddAction: ActionFunction = async ({ request }) => {
    const data = await request.formData();
    const response = await simplePOSTFetch<SystemUser>(ENDPOINT.USERS_ADD, data);

    if (response.status === 200) return redirect(`/user/${response.data.id}`);
    return response.status;
}