import { Box, Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { Form, useActionData, useLoaderData } from "react-router-dom";
import { SpecialitiesSelect } from "../Components";
import { Appointment } from "../Logic/appointments";

const AppointmentsBookPage = () => {
    const appointments = useLoaderData() as Array<Appointment>;
    const error = useActionData();

    return <Box p={2}>
        <Form>
            <SpecialitiesSelect />
            <Button type="submit">Filter</Button>
        </Form>
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Doctor</TableCell>
                        <TableCell align="right">Date</TableCell>
                        <TableCell align="right">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {appointments.map((appointment) => (
                        <TableRow
                            key={appointment.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {appointment.doctor.fullName}
                            </TableCell>
                            <TableCell align="right">{new Date(appointment.startTime).toLocaleString()}</TableCell>
                            <TableCell align="right">
                                <Form method="POST">
                                    <input value={appointment.id} name={"Id"} hidden />
                                    <Button type="submit" color={error ? "error" : "primary"}>Book</Button>
                                </Form>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    </Box>
}

export default AppointmentsBookPage;