import { Box, Button, Paper, Stack, TextField, Typography } from "@mui/material";
import { Form, useActionData, useLoaderData } from "react-router-dom";
import { Appointment } from "../Logic/appointments";
import { ActionResultMessage } from "../Components";

const AppointmentsDetailsPage = () => {
    const appointment = useLoaderData() as Appointment;
    const status = useActionData() as number | undefined;

    return <Box p={2} maxWidth={"1000px"} m="auto">
        <Stack spacing={2}>
            <Typography variant="h3">Appointment</Typography>
            <ActionResultMessage status={status} />
            <Form method="post">
                <input name="Id" value={appointment.id} hidden />
                <TextField defaultValue={appointment.description} name={"description"} label="description" />
                <Button type="submit">Edit</Button>
            </Form>
            <Typography variant="h6">Doctor: {appointment.doctor.fullName}</Typography>
            <Typography variant="h6">Patient: {appointment.patient.fullName}</Typography>
            <Typography variant="h6">Date: {new Date(appointment.startTime).toLocaleString()}</Typography>
        </Stack>
    </Box >
}

export default AppointmentsDetailsPage;