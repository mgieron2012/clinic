import { Box, Button, TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Tab, Tabs, ButtonBase, Grid } from "@mui/material";
import { Form, useActionData, useFetcher, useLoaderData, useNavigate } from "react-router-dom";
import { Appointments } from "../Logic/appointments";

const AppointmentsList = () => {
    const error = useActionData();
    const fetcher = useFetcher();
    const { appointments, history } = useLoaderData() as { appointments: Appointments, history: boolean };

    const navigate = useNavigate();

    return <Box p={2}>
        <Tabs centered value={history ? 0 : 1}>
            <ButtonBase onClick={() => navigate("/appointments?history=true")}>
                <Tab label={"History"} />
            </ButtonBase>
            <ButtonBase onClick={() => navigate("/appointments?history=false")}>
                <Tab label={"Incoming"} />
            </ButtonBase>
        </Tabs>
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Doctor</TableCell>
                        <TableCell align="right">Date</TableCell>
                        <TableCell align="right">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {appointments.map((appointment) => (
                        <TableRow
                            key={appointment.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {appointment.doctor.fullName}
                            </TableCell>
                            <TableCell align="right">{new Date(appointment.startTime).toLocaleString()}</TableCell>
                            <TableCell align="right">
                                <Grid container>
                                    <Grid xs={6}>
                                        {history ||
                                            <Form method="POST">
                                                <input value={appointment.id} name={"Id"} hidden />
                                                <Button type="submit" fullWidth variant="outlined">Cancel</Button>
                                            </Form>
                                        }
                                    </Grid>
                                    <Grid xs={6}>
                                        <Button onClick={() => navigate(`/appointment/${appointment.id}`)} fullWidth variant="outlined">Edit</Button>
                                    </Grid>
                                </Grid>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        <Button onClick={() => navigate("/appointments/book")} fullWidth sx={{ mt: 2}} variant="outlined">Book appointment</Button>
    </Box>
}

export default AppointmentsList;