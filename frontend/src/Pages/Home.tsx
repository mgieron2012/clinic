import { Box, AppBar, Toolbar, IconButton, Typography, Button } from "@mui/material";
import { Outlet, useNavigate } from "react-router-dom";
import user from "../Logic/user";

const HomePage = () => {
    const navigate = useNavigate();

    return (
        <Box>
            <Box flexGrow={1}>
                <AppBar position="static">
                    <Toolbar>
                        <div style={{ flexGrow: 1 }}>
                            <Button onClick={() => navigate("/users")} color="inherit">
                                Users
                            </Button> 
                            <Button onClick={() => navigate("/schedules")} color="inherit">
                                Schedules
                            </Button>
                            <Button onClick={() => navigate("/report")} color="inherit">
                                Report
                            </Button>
                            <Button onClick={() => navigate("/appointments")} color="inherit">
                                Appointments
                            </Button>
                        </div>
                        {user.isAuthorized ?
                            <Button onClick={() => { user.signOut(); navigate("/"); }} color="inherit">
                                Sign out
                            </Button> : <>
                                <Button onClick={() => navigate("/signIn")} color="inherit">
                                    Sign in
                                </Button>
                                <Button onClick={() => navigate("/signUp")} color="inherit">
                                    Sign up
                                </Button></>
                        }
                    </Toolbar>
                </AppBar>
            </Box>
            <Outlet />
        </Box>
    );
}

export default HomePage;