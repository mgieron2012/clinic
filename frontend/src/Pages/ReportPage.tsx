import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Button, TextField, Box, Stack } from "@mui/material";
import { Form, useLoaderData } from "react-router-dom";
import { Report } from "../Logic/report";

const ReportPage = () => {
    const report = useLoaderData() as Report;

    return (
        <Box p={2}>
            <Box p={2}>
                <Form>
                    <TextField type="date" name="from" defaultValue={report.from} label="Start day" />
                    <TextField type="date" name="to" defaultValue={report.to} label="End day" />
                    <Button type="submit" sx={{ mt: 1 }}>Create report</Button>
                </Form>
            </Box>
            <Stack spacing={2}>
                {Object.entries(report.schedules).map(([key, schedules]) =>
                    <>{schedules.length > 0 &&
                        <TableContainer component={Paper}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell colSpan={4} align="center">
                                            {new Date(key).toLocaleDateString()}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Doctor</TableCell>
                                        <TableCell align="right">Start time</TableCell>
                                        <TableCell align="right">End time</TableCell>
                                        <TableCell align="right">Appointments</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        schedules.map(schedule =>
                                            <TableRow
                                                key={schedule.id}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {schedule.doctor.fullName}
                                                </TableCell>
                                                <TableCell align="right">{new Date(schedule.startTime).toLocaleTimeString()}</TableCell>
                                                <TableCell align="right">{new Date(schedule.endTime).toLocaleTimeString()}</TableCell>
                                                <TableCell align="right">
                                                    {report.appointmentsCount[schedule.id]} / { }
                                                    {Math.floor((new Date(schedule.endTime).getTime() - new Date(schedule.startTime).getTime()) / (1000 * 60 * 15))}
                                                </TableCell>
                                            </TableRow>
                                        )
                                    }

                                </TableBody>
                            </Table>
                        </TableContainer>
                    }
                    </>
                )}
            </Stack>
        </Box>
    );
}

export default ReportPage;