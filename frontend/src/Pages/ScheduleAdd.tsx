import { Box, Stack, Typography, TextField, Button, FormControl, InputLabel, Select } from "@mui/material";
import { useActionData, Form, useLoaderData } from "react-router-dom";
import { ActionResultMessage } from "../Components";
import { SystemUser } from "../Logic/users";

const ScheduleAddPage = () => {
    const users = useLoaderData() as Array<SystemUser>;
    const status = useActionData() as number;

    return <Box maxWidth={"80vw"} m={"auto"} p={2}>

        <Form method="post">
            <Stack spacing={3}>
                <Typography variant="h4">Create schedule</Typography>
                <ActionResultMessage status={status} />

                <TextField name="Date" type="date" label="Date" fullWidth required />
                <TextField name="StartTime" type="time" label="Start time" fullWidth required />
                <TextField name="EndTime" type="time" label="End time" fullWidth required />

                <FormControl fullWidth>
                    <InputLabel shrink htmlFor="doctor-select">
                        Doctor
                    </InputLabel>
                    <Select native name="DoctorId" label="Doctor" id="doctor-select">
                        {users.filter(u => u.isDoctor).map(({ id, fullName }) =>
                            <option value={id}>{fullName}</option>
                        )}
                    </Select>
                </FormControl>

                <Button type="submit" variant="contained">Create</Button>
            </Stack>
        </Form>
    </Box>
}

export default ScheduleAddPage;