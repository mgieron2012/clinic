import { Form, useActionData, useLoaderData } from "react-router-dom";
import { ActionResultMessage } from "../Components";
import { Box, Button, FormControl, InputLabel, Select, Stack, TextField } from "@mui/material";
import { Schedule } from "../Logic/schedules";
import { SystemUser } from "../Logic/users";

const ScheduleDetails = () => {
    const { schedule, users } = useLoaderData() as { schedule: Schedule, users: Array<SystemUser> };
    const status = useActionData() as number;

    return <Box maxWidth={"80vw"} m={"auto"} p={2}>
        <Form method="post">
            <Stack spacing={3}>
                <ActionResultMessage status={status} />
                <input name="Id" defaultValue={schedule.id} hidden />
                <TextField name="Date" type="date" label="Date" fullWidth required defaultValue={new Date(schedule.date).toISOString().substring(0, 10)} />
                <TextField name="StartTime" type="time" label="Start time" fullWidth required defaultValue={new Date(schedule.startTime).toLocaleTimeString()} />
                <TextField name="EndTime" type="time" label="End time" fullWidth required defaultValue={new Date(schedule.endTime).toLocaleTimeString()} />

                <FormControl fullWidth>
                    <InputLabel shrink htmlFor="doctor-select">
                        Doctor
                    </InputLabel>
                    <Select native name="DoctorId" label="Doctor" id="doctor-select" defaultValue={schedule.doctorId}>
                        {users.filter(u => u.isDoctor).map(({ id, fullName }) =>
                            <option value={id}>{fullName}</option>
                        )}
                    </Select>
                </FormControl>

                <Button type="submit" name="intent" value="edit" variant="contained">Save</Button>
                <Button type="submit" color="error" name="intent" value="delete" variant="contained">Delete</Button>
            </Stack>
        </Form>
    </Box>
}

export default ScheduleDetails;