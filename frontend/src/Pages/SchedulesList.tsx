import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Button, TextField, Box } from "@mui/material";
import { Form, Link, useFetcher, useLoaderData } from "react-router-dom";
import { Schedule } from "../Logic/schedules";

const SchedulesListPage = () => {
    const { schedules, date } = useLoaderData() as { schedules: Array<Schedule>, date: string };
    const fetcher = useFetcher();

    return (
        <Box p={2}>
            <Box p={2}>
                <Form>
                    <TextField type="date" name="date" defaultValue={date} />
                    <Button type="submit" sx={{ mt: 1 }}>Filter</Button>
                </Form>
                <fetcher.Form method="post" action='/schedules/copy'>
                    <TextField type="date" name="fromDay" defaultValue={date} label="source day" />
                    <TextField type="date" name="toDay" defaultValue={date} label="destination day" />
                    <Button type="submit" sx={{ mt: 1 }} disabled={fetcher.state !== "idle"}
                        color={fetcher.data === 200 ? "success" : fetcher.data === 400 ? "error" : "primary"}>Copy</Button>
                </fetcher.Form>
            </Box>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Doctor</TableCell>
                            <TableCell align="right">Start time</TableCell>
                            <TableCell align="right">End time</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {schedules.map((schedule) => (
                            <TableRow
                                key={schedule.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {schedule.doctor.fullName}
                                </TableCell>
                                <TableCell align="right">{new Date(schedule.startTime).toLocaleTimeString()}</TableCell>
                                <TableCell align="right">{new Date(schedule.endTime).toLocaleTimeString()}</TableCell>
                                <TableCell align="right">
                                    <Link to={`/schedule/${schedule.id}`}>Edit</Link>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Button variant="contained" sx={{ m: 2 }}>
                <Link to="add">Add schedule</Link>
            </Button>
        </Box>
    );
}

export default SchedulesListPage;