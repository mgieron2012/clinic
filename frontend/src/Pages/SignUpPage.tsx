import React from "react"
import { Avatar, Box, Button, CssBaseline, Grid, Paper, Typography, TextField, Alert } from "@mui/material";
import { Form, Link, useActionData, useNavigation } from "react-router-dom";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const SignUpPage = () => {
    const navigation = useNavigation();
    const error = useActionData();

    return <>
        <Grid container component="main" sx={{ height: '100vh' }}>
            <CssBaseline />
            <Grid
                item
                xs={false}
                sm={4}
                md={7}
                sx={{
                    backgroundImage: 'url(https://source.unsplash.com/random?hospital)',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                }}
            />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <Box
                    sx={{
                        my: 8,
                        mx: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                        {error === 451 &&
                            <Alert severity="error">This email is taken</Alert>}
                        <Form method="post">
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="FirstName"
                                label="First Name"
                                name="FirstName"
                                autoFocus
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="LastName"
                                label="Last Name"
                                name="LastName"
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="Email"
                                type="email"
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="Password"
                                label="Password"
                                type="password"
                                id="password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                disabled={navigation.state !== "idle"}
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Sign Up
                            </Button>
                        </Form>
                        <Grid container>
                            <Grid item>
                                <Link to="/signIn">
                                    {"Already have an account? Sign In"}
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    </>
}

export default SignUpPage;