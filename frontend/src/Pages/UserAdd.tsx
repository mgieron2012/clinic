import { Box, Stack, TextField, FormControlLabel, Checkbox, Button, Typography } from "@mui/material";
import { Form, useActionData } from "react-router-dom";
import { ActionResultMessage, SpecialitiesSelect } from "../Components";

const UserAdd = () => {
    const status = useActionData() as number;

    return <Box maxWidth={"80vw"} m={"auto"} p={2}>

        <Form method="post">
            <Stack spacing={3}>
                <Typography variant="h4">Create doctor account</Typography>
                <ActionResultMessage status={status} />
                
                <TextField name="FirstName" label="First Name" fullWidth required />
                <TextField name="LastName" label="Last Name" fullWidth required />
                <TextField name="Email" type="email" label="Email" fullWidth required />
                <TextField name="Password" type="password" label="Password" fullWidth required />
                <SpecialitiesSelect />
                <Button type="submit" name="intent" value="edit" variant="contained">Create</Button>
            </Stack>
        </Form>
    </Box>
}

export default UserAdd;