import { Form, useActionData, useLoaderData } from "react-router-dom";
import { SystemUser } from "../Logic/users";
import { ActionResultMessage, SpecialitiesSelect } from "../Components";
import { Box, Button, Checkbox, FormControlLabel, Stack, TextField } from "@mui/material";

const UserDetails = () => {
    const user = useLoaderData() as SystemUser;
    const status = useActionData() as number;

    return <Box maxWidth={"80vw"} m={"auto"} p={2}>
        <Form method="post">
            <Stack spacing={3}>
                <ActionResultMessage status={status} />
                <input name="Id" defaultValue={user.id} hidden />
                <TextField name="FirstName" defaultValue={user.firstName} label="First Name" fullWidth required />
                <TextField name="LastName" defaultValue={user.lastName} label="Last Name" fullWidth required />
                <TextField name="Email" type="email" defaultValue={user.email} label="Email" fullWidth required />
                <FormControlLabel control={
                    <Checkbox defaultChecked={user.isActive} name="IsActive" value={true} />
                } label="Activated" />
                {user.isDoctor &&
                    <SpecialitiesSelect defaultValue={user.specialityId} />
                }
                <Button type="submit" name="intent" value="edit" variant="contained">Save</Button>
                <Button type="submit" color="error" name="intent" value="delete" variant="contained">Delete</Button>
            </Stack>
        </Form>
    </Box>
}

export default UserDetails;