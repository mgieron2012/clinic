import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Button, Tabs, Tab } from "@mui/material";
import { Link, useFetcher, useLoaderData } from "react-router-dom";
import { SystemUser } from "../Logic/users";
import { useState } from "react";

const UsersListPage = () => {
    const users = useLoaderData() as Array<SystemUser>;
    const fetcher = useFetcher();
    const [tab, setTab] = useState(1);

    return (
        <>
            <Tabs value={tab || 1} centered onChange={(_, newValue: number) => setTab(newValue)}>
                <Tab label={"Patients"} value={1} tabIndex={1} />
                <Tab label={"Doctors"} value={2} tabIndex={2} />
            </Tabs>
            <TableContainer component={Paper}>
                {tab === 1 ?
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Full Name</TableCell>
                                <TableCell align="right">Email</TableCell>
                                <TableCell align="right">Active</TableCell>
                                <TableCell align="right">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.filter(user => !user.isDoctor).map((user) => (
                                <TableRow
                                    key={user.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {user.fullName}
                                    </TableCell>
                                    <TableCell align="right">{user.email}</TableCell>
                                    <TableCell align="right">
                                        {user.isActive ? "Activated" :
                                            <fetcher.Form method="POST">
                                                <input hidden name="id" value={user.id}></input>
                                                <Button type="submit" disabled={fetcher.state !== "idle"}>Activate</Button>
                                            </fetcher.Form>
                                        }
                                    </TableCell>
                                    <TableCell align="right">
                                        <Link to={`/user/${user.id}`}>Edit</Link>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    :
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Full Name</TableCell>
                                <TableCell align="right">Email</TableCell>
                                <TableCell align="right">Speciality</TableCell>
                                <TableCell align="right">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.filter(user => user.isDoctor).map((user) => (
                                <TableRow
                                    key={user.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {user.fullName}
                                    </TableCell>
                                    <TableCell align="right">{user.email}</TableCell>
                                    <TableCell align="right">{user.speciality.name}</TableCell>
                                    <TableCell align="right">
                                        <Link to={`/user/${user.id}`}>Edit</Link>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                }
            </TableContainer>
            <Button variant="contained" sx={{ m: 2 }}>
                <Link to="add">Add doctors account</Link>
            </Button>
        </>
    );
}

export default UsersListPage;