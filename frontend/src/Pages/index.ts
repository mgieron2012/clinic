import HomePage from "./Home";
import SignInPage from "./SignInPage";
import SignUpPage from "./SignUpPage";
import UsersListPage from "./UsersList";
import UserDetailsPage from "./UserDetails";
import UserAddPage from "./UserAdd";
import SchedulesListPage from "./SchedulesList";
import ScheduleAddPage from "./ScheduleAdd";
import ScheduleDetailsPage from "./ScheduleDetails";
import ReportPage from "./ReportPage";
import AppointmentsListPage from "./AppointmentsListPage";
import AppointmentsBookPage from "./AppointmentsBookPage";
import AppointmentsDetailsPage from "./AppointmentsDetailsPage";

export {
    HomePage,
    SignInPage,
    SignUpPage,
    UsersListPage,
    UserDetailsPage,
    UserAddPage,
    SchedulesListPage,
    ScheduleAddPage,
    ScheduleDetailsPage,
    ReportPage,
    AppointmentsListPage,
    AppointmentsBookPage,
    AppointmentsDetailsPage
}