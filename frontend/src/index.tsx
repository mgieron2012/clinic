import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { CssBaseline } from '@mui/material';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { ThemeProvider, createTheme } from '@mui/material/styles';

import { HomePage, SignInPage, SignUpPage, UsersListPage, UserDetailsPage, UserAddPage, SchedulesListPage, ScheduleAddPage, ScheduleDetailsPage, ReportPage, AppointmentsListPage, AppointmentsBookPage, AppointmentsDetailsPage } from './Pages';
import { signInAction, signUpAction } from './Logic/auth';
import { activationAction, userAction, userAddAction, userLoader, usersLoader } from './Logic/users';
import { scheduleAction, scheduleAddAction, scheduleLoader, schedulesLoader, scheduleCopyAction } from './Logic/schedules';
import { reportLoader } from './Logic/report';
import { appointmentBookAction, appointmentCancelAction, appointmentEditAction, appointmentLoader, appointmentsFilterLoader, appointmentsLoader } from './Logic/appointments';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const router = createBrowserRouter([
  {
    path: "/signIn",
    element: <SignInPage />,
    action: signInAction
  },
  {
    path: "/signUp",
    element: <SignUpPage />,
    action: signUpAction
  },
  {
    path: "/",
    element: <HomePage />,
    children: [
      {
        path: "/users",
        element: <UsersListPage />,
        loader: usersLoader,
        action: activationAction
      },
      {
        path: "/user/:userId",
        element: <UserDetailsPage />,
        loader: userLoader,
        action: userAction
      },
      {
        path: "/users/add",
        element: <UserAddPage />,
        action: userAddAction
      },
      {
        path: "/schedules",
        element: <SchedulesListPage />,
        loader: schedulesLoader
      },
      {
        path: "/schedules/add",
        element: <ScheduleAddPage />,
        action: scheduleAddAction,
        loader: usersLoader
      },
      {
        path: "/schedule/:scheduleId",
        element: <ScheduleDetailsPage />,
        action: scheduleAction,
        loader: scheduleLoader
      },
      {
        path: "/schedules/copy",
        action: scheduleCopyAction
      },
      {
        path: "/report",
        element: <ReportPage />,
        loader: reportLoader
      },
      {
        path: "/appointments",
        element: <AppointmentsListPage />,
        loader: appointmentsLoader,
        action: appointmentCancelAction
      },
      {
        path: "/appointments/book",
        element: <AppointmentsBookPage />,
        loader: appointmentsFilterLoader,
        action: appointmentBookAction
      },
      {
        path: "/appointment/:appointmentId",
        element: <AppointmentsDetailsPage />,
        loader: appointmentLoader,
        action: appointmentEditAction
      }
    ]
  },
]);

root.render(
  <React.StrictMode>
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <RouterProvider router={router} />
    </ThemeProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
